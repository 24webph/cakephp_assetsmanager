<?php
namespace AssetsManager\Model\Entity;

use Cake\ORM\Entity;

/**
 * Asset Entity
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $local
 * @property float $file
 * @property int|null $asset_type_id
 * @property string $model
 * @property int|null $foreign_key
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\AssetType $asset_type
 */
class Asset extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'local' => true,
        'file' => true,
        'asset_type_id' => true,
        'model' => true,
        'foreign_key' => true,
        'created' => true,
        'modified' => true,
        'asset_type' => true,
        
    ];
}
