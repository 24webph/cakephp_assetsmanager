<?php
use Migrations\AbstractSeed;
use Cake\I18n\Time;
/**
 * AssetTypes seed.
 */
class AssetTypesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'Document',
                'active' => '1',
                'created' => Time::now(),
                'modified' => Time::now(),
            ],
            [
                'id' => '2',
                'name' => 'Image',
                'active' => '1',
                'created' => Time::now(),
                'modified' => Time::now(),
            ],
            [
                'id' => '3',
                'name' => 'Other',
                'active' => '1',
                'created' => Time::now(),
                'modified' => Time::now(),
            ],
        ];

        $table = $this->table('asset_types');
        $table->insert($data)->save();
    }
}
