<?php

namespace AssetsManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use SplFileObject;

/**
 * Assets Model
 *
 * @property \App\Model\Table\AssetTypesTable|\Cake\ORM\Association\BelongsTo $AssetTypes
 *
 * @method \App\Model\Entity\Asset get($primaryKey, $options = [])
 * @method \App\Model\Entity\Asset newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Asset[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Asset|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Asset|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Asset patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Asset[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Asset findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AssetsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('assets');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'file' => [
                'path' => 'webroot{DS}files{DS}{model}{DS}assets{DS}',
                'deleteCallback' => function ($path, $entity, $field, $settings) {
                    // When deleting the entity, both the original and the thumbnail will be removed
                    // when keepFilesOnDelete is set to false
                    $event = new Event('Model.Assets.beforeDeleteCallback', $this, ['entity' => $entity]);
                    $this->getEventManager()->dispatch($event);
                    if (!empty($event->getResult()['asset'])) {
                        $eventResult = $event->getResult()['asset'];
                    }
                    // Now return the original *and* the thumbnail
                    return array_merge([$path . $entity->{$field}], $eventResult);
                },
                'keepFilesOnDelete' => false,
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $name = pathinfo($data['name'], PATHINFO_FILENAME);
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);
                    return strtolower($name . '_' . uniqid().'.'.$extension);
                },
                'transformer' => function ($table, $entity, $data, $field, $settings) {

                    $result=[$data['tmp_name'] => $data['name']];
                    $event = new Event('Model.Assets.beforeTransformer', $this, ['data' => $data]);
                    $this->getEventManager()->dispatch($event);
                    $eventResult = null;
                    if (!empty($event->getResult())) {
                        $result = $event->getResult();
                    }
                    
                    // Now return the original *and* the thumbnail
                    return $result ;
                },
            ]
        ]);


        $this->belongsTo('AssetTypes', [
            'foreignKey' => 'asset_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('name')
                ->maxLength('name', 100)
                ->requirePresence('name', 'create')
                ->notEmpty('name');

        $validator
                ->scalar('description')
                ->maxLength('description', 200)
                ->requirePresence('description', 'create')
                ->notEmpty('description');

        $validator
                ->requirePresence('file', 'create')
                ->allowEmpty('file');

        $validator
                ->scalar('model')
                ->maxLength('model', 100)
                ->requirePresence('model', 'create')
                ->notEmpty('model');

        $validator
                ->integer('foreign_key')
                ->allowEmpty('foreign_key');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['asset_type_id'], 'AssetTypes'));
        return $rules;
    }

}
