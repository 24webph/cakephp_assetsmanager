<?php

/**
 * AttachmentBehavior
 *
 * PHP version 7
 * 
 * @category Behaviour
 * @package  24WEB.Attachment
 * @version  V1
 * @author   Paulo Homem <paulo.homem@24web.pt>
 * @license  
 * @link     http://24web.pt
 */

namespace AssetsManager\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\Event\Event;
use ArrayObject;

class AttachmentBehavior extends Behavior {

    public function initialize(array $config) {
        $this->setTableSettings();
    }

    // In a table or behavior class
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options) {
        if (isset($data['assets'])) {
            $data = $this->fillDefaultValues($data);
            $data = $this->deleteRemoved($data);
            $data = $this->removeEmpty($data);
        }
    }

    private function fillDefaultValues($data) {

        if ($this->hasStringKeys($data['assets'])) {
            $data = $this->fillDefaultValuesObject($data);
        } else {
            $data = $this->fillDefaultValuesArray($data);
        }

        return $data;
    }

    private function fillDefaultValuesObject($data) {
        if (!isset($data['assets']['model'])) {
            $data['assets']['model'] = $this->getTable()->getAlias();
        }
        if (!isset($data['assets']['name'])) {
            $data['assets']['name'] = $data['assets']['file']['name'];
        }
        if (!isset($data['assets']['description'])) {
            $data['assets']['description'] = __('Attached');
        }
        return $data;
    }

    private function fillDefaultValuesArray($data) {
        foreach ($data['assets'] as $key => $value) {
            if (!isset($data['assets'][$key]['model'])) {
                $data['assets'][$key]['model'] = $this->getTable()->getAlias();
            }
            if (!isset($data['assets'][$key]['name'])) {
                $data['assets'][$key]['name'] = $data['assets'][$key]['file']['name'];
            }
            if (!isset($data['assets'][$key]['description'])) {
                $data['assets'][$key]['description'] = __('Attached');
            }
        }
        return $data;
    }

    function deleteRemoved($data) {
        if ($this->hasStringKeys($data['assets'])) {
            $data = $this->deleteRemovedObject($data);
        } else {
            $data = $this->deleteRemovedArray($data);
        }

        return $data;
    }

    private function deleteRemovedObject($data) {
        if (isset($data['assets']['id']) && $data['assets']['id'] != '' && isset($data['assets']['removed']) && $data['assets']['removed'] == 1) {
            $entity = $this->getTable()->Assets->get($data['assets']['id']);
            $result = $this->getTable()->Assets->delete($entity);
        }
        return $data;
    }

    private function deleteRemovedArray($data) {
        foreach ($data['assets'] as $key => $value) {
            if (isset($data['assets'][$key]['id']) && $data['assets'][$key]['id'] != '' && isset($data['assets'][$key]['removed']) && $data['assets'][$key]['removed'] == 1) {
                $entity = $this->getTable()->Assets->get($data['assets'][$key]['id']);
                $result = $this->getTable()->Assets->delete($entity);
            }
        }
        return $data;
    }

    function removeEmpty($data) {
        if ($this->hasStringKeys($data['assets'])) {
            $data = $this->removeEmptyObject($data);
        } else {
            $data = $this->removeEmptyArray($data);
        }
        if ($data['assets'] == []) {
            unset($data['assets']);
        }
        return $data;
    }

    function removeEmptyObject($data) {
        $value = $data['assets'];
        if ($value['file']['error'] == 4) {
            unset($data['assets']);
        }
        return $data;
    }

    function removeEmptyArray($data) {
        foreach ($data['assets'] as $key => $value) {
            if ($value['file']['error'] == 4) {
                unset($data['assets'][$key]);
            }
        }
        return $data;
    }

    private function setTableSettings() {
        $this->getTable()->hasMany('AssetsManager.Assets')
                ->setForeignKey('foreign_key')
                ->conditions(['model' => $this->getTable()->getAlias()]);
        //return;

        $this->getTable()->Assets->getValidator()->provider('upload', \Josegonzalez\Upload\Validation\UploadValidation::class);
        // OR
        //$this->getTable()->getValidator()->provider('upload', \Josegonzalez\Upload\Validation\ImageValidation::class);
        // OR
        //$this->getTable()->getValidator()->provider('upload', \Josegonzalez\Upload\Validation\DefaultValidation::class);

        $this->getTable()->Assets->getValidator()->add('file', 'fileUnderPhpSizeLimit', [
            'rule' => 'isUnderPhpSizeLimit',
            'message' => 'This file is too large',
            'provider' => 'upload'
        ]);
        $this->getTable()->Assets->getValidator()->add('file', 'fileUnderFormSizeLimit', [
            'rule' => 'isUnderFormSizeLimit',
            'message' => 'This file is too large',
            'provider' => 'upload'
        ]);
        $this->getTable()->Assets->getValidator()->add('file', 'fileCompletedUpload', [
            'rule' => 'isCompletedUpload',
            'message' => 'This file could not be uploaded completely',
            'provider' => 'upload'
        ]);
        $this->getTable()->Assets->getValidator()->add('file', 'fileFileUpload', [
            'rule' => 'isFileUpload',
            'message' => 'There was no file found to upload',
            'provider' => 'upload'
        ]);
        $this->getTable()->Assets->getValidator()->add('file', 'fileSuccessfulWrite', [
            'rule' => 'isSuccessfulWrite',
            'message' => 'This upload failed',
            'provider' => 'upload'
        ]);
        $this->getTable()->Assets->getValidator()->add('file', 'fileBelowMaxSize', [
            'rule' => ['isBelowMaxSize', 15000000],
            'message' => 'This file is too large',
            'provider' => 'upload',
            'on' => function($context) {
                return !empty($context['data']['file']) && $context['data']['file']['error'] == UPLOAD_ERR_OK;
            }
        ]);
        $this->getTable()->Assets->getValidator()->add('file', 'fileAboveMinSize', [
            'rule' => ['isAboveMinSize', 100],
            'message' => 'This file is too small',
            'provider' => 'upload',
            'on' => function($context) {

                return !empty($context['data']['file']) && $context['data']['file']['error'] == UPLOAD_ERR_OK;
            }
        ]);
    }

    private function hasStringKeys(array $array) {
        return count(array_filter(array_keys($array), 'is_string')) > 0;
    }

}
