(function ($) {
    $.extend({
        uploadPreview24web: function (options) {

            // Options + Defaults
            var settings = $.extend({
                input_field: ".image-input",
                preview_box: ".image-preview",
                clear_button: ".clear-button",
                label_field: ".image-label",
                label_default: "Choose File",
                label_selected: "Change File",
                no_label: false,
                background_image: "",
                success_callback: null,
                removed_callback: null
            }, options);

            // Check if FileReader is available
            if (window.File && window.FileList && window.FileReader) {
                if (typeof ($(settings.input_field)) !== 'undefined' && $(settings.input_field) !== null) {
                    $(settings.input_field).on('change', function () {
                        var files = this.files;

                        if (files.length > 0) {
                            var file = files[0];
                            var reader = new FileReader();

                            // Load file
                            reader.addEventListener("load", function (event) {
                                var loadedFile = event.target;

                                // Check format
                                if (file.type.match('image')) {
                                    switch ($(settings.preview_box).prop("tagName")) {
                                        case 'DIV':
                                            // Image
                                            $(settings.preview_box).css("background-image", "url(" + loadedFile.result + ")");
                                            $(settings.preview_box).css("background-size", "cover");
                                            $(settings.preview_box).css("background-position", "center center");
                                            break;
                                        case 'IMG':
                                            $(settings.preview_box).attr('src', loadedFile.result);
                                            break;
                                    }


                                    // is img
                                } else if (file.type.match('audio')) {
                                    // Audio
                                    $(settings.preview_box).html("<audio controls><source src='" + loadedFile.result + "' type='" + file.type + "' />Your browser does not support the audio element.</audio>");
                                } else {
                                    alert("This file type is not supported yet.");
                                }
                            });

                            if (settings.no_label === false) {
                                // Change label
                                $(settings.label_field).html(settings.label_selected);
                            }

                            // Read the file
                            reader.readAsDataURL(file);

                            // Success callback function call
                            if (settings.success_callback) {
                                settings.success_callback();
                            }
                        } else {
                            if (settings.no_label === false) {
                                // Change label
                                $(settings.label_field).html(settings.label_default);
                            }
                            // Clear background
                            //$(settings.preview_box).css("background-image", "none");
                            switch ($(settings.preview_box).prop("tagName")) {
                                case 'DIV':
                                    // Image
                                    $(settings.preview_box).css("background-image", "url('" + settings.background_image + "')");
                                    $(settings.preview_box).css("background-size", "cover");
                                    $(settings.preview_box).css("background-position", "center center");
                                    break;
                                case 'IMG':
                                    $(settings.preview_box).attr('src', settings.background_image);
                                    break;
                            }

                            // Remove Audio
                            $(settings.preview_box).children("audio").remove();
                        }
                    });
                }
                $(settings.preview_box).on('click', function () {
                    $(settings.input_field).trigger('click');
                });
                $(settings.clear_button).on('click', function () {
                    $(settings.input_field).val(null);
                    $(settings.input_field).trigger('change');
                    // Removed callback function call
                    if (settings.removed_callback) {
                        settings.removed_callback();
                    }
                })

            } else {
                alert("You need a browser with file reader support, to use this form properly.");
                return false;
            }
        }
    });
})(jQuery);
