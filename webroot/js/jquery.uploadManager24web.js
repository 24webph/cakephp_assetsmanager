(function ($) {



    $.fn.uploadManager24web = function () {

        this.each(function (index) {
            var input_field = $(this);
            var preview_box = $(this).parents('.thumbnail-content').find('.img-thumbnail');
            var clear_button = $(this).parents('.thumbnail-content').find('button.clear-thumbnail');
            $.uploadPreview24web({
                input_field: input_field, // Default: .image-upload
                preview_box: preview_box, // Default: .image-preview
                clear_button: clear_button, // Default: .clear-button
                no_label: true, // Default: false
                background_image: '/assets_manager/img/default-thumbnail.jpg', // Default: null
                removed_callback: setRemoved, // Default: null
                success_callback: setLoaded // Default: null

            });
            function setRemoved() {
                $(input_field).parents('.thumbnail-content').find('input.removed_thumbnail').val(1);
            }
            function setLoaded() {
                $(input_field).parents('.thumbnail-content').find('input.removed_thumbnail').val(0);
            }


        });



    }

}(jQuery));